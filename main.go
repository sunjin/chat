package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"text/template"

	"github.com/stretchr/gomniauth"
	"github.com/stretchr/gomniauth/providers/facebook"
	"github.com/stretchr/gomniauth/providers/github"
	"github.com/stretchr/gomniauth/providers/google"
	"github.com/stretchr/objx"
)

// global
var avatars Avatar = TryAvatars{
	UserFileSystemAvatar,
	UseAuthAvatar,
	UserGravatarAvatar,
}

type templateHandler struct {
	once     sync.Once
	filename string
	templ    *template.Template
}

func (t *templateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t.once.Do(func() {
		t.templ = template.Must(template.ParseFiles(filepath.Join("templates", t.filename)))
	})

	data := map[string]interface{}{
		// "Host": r.Host,
		"Host": getParamString("DOMAIN", "localhost:8080"),
	}

	if authCookie, err := r.Cookie("auth"); err == nil {
		data["UserData"] = objx.MustFromBase64(authCookie.Value)
	}

	t.templ.Execute(w, data)
}

// 環境変数からデータを取得する、もし存在しない場合はデフォルトの値を指定する
func getParamString(param string, defaultValue string) string {
	env := os.Getenv(param)
	if env != "" {
		return env
	}
	return defaultValue
}

func main() {

	log.Println("=== Server Start ===")

	// 引数でportを指定
	var addr = flag.String("addr", ":8080", "アプリケーションのアドレス")
	flag.Parse()

	// Gominiauthをセットアップ
	gomniauth.SetSecurityKey("suususuuusususuuu")
	gomniauth.WithProviders(
		facebook.New("447160019339549", "26f3f97920193a35c495e4b713978745", "https://sunjin.info/auth/callback/facebook"),
		github.New("ebf06d44f96704ee24db", "a6c703af548092d78038c916277069abd6614bd1", "https://sunjin.info/auth/callback/github"),
		google.New("459086926375-ej8qm18irticmqsod4dbrhqu649fap2c.apps.googleusercontent.com", "uA44D-BIjOgdEFruWYKDMk9L", "https://sunjin.info/auth/callback/google"),
	)

	// room instanceを生成
	r := newRoom()

	// root
	// http.Handle("/", &templateHandler{filename: "chat.html"})
	http.Handle("/", &templateHandler{filename: "index.html"})
	http.Handle("/policy", &templateHandler{filename: "policy.html"})
	http.Handle("/chat", MustAuth(&templateHandler{filename: "chat.html"}))
	http.Handle("/room", r)
	http.Handle("/login", &templateHandler{filename: "login.html"})
	http.HandleFunc("/auth/", loginHandler)
	http.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		http.SetCookie(w, &http.Cookie{
			Name:   "auth",
			Value:  "",
			Path:   "/",
			MaxAge: -1,
		})
		w.Header()["Location"] = []string{"/chat"}
		w.WriteHeader(http.StatusTemporaryRedirect)
	})
	http.Handle("/public/", http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))
	http.Handle("/upload", &templateHandler{filename: "upload.html"})
	http.HandleFunc("/uploader", uploaderHandler)
	http.Handle("/avatars/", http.StripPrefix("/avatars/", http.FileServer(http.Dir("./avatars"))))

	go r.run()

	log.Println("Webサーバーを起動します。ポート:", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("err", err)
	}

}
