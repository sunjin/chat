package main

import (
	"errors"
	"io/ioutil"
	"path/filepath"
)

// ErrNoAvatarURL はAvatarインスタンスがアバターのURLを返すことができない場合に発生する例外
var ErrNoAvatarURL = errors.New("chat: アバターのURLを取得できません")

// Avatar はユーザーのプロフィール画像を表す型です
type Avatar interface {
	// GetAvatarURL は指定されたクライアントのアバターのURLを返します
	// 問題が発生した場合にはエラーを返します。特に、URLを取得できなかった場合は
	// ErrNoAvatarURLを返します
	GetAvatarURL(u ChatUser) (string, error)
}

// AuthAvatar は通常通りアカウントからバナーを取得する場合に使用するものです
type AuthAvatar struct{}

// TryAvatars はAvatarオブジェクトのslice
type TryAvatars []Avatar

// GetAvatarURL は3つのthumbの取得を試して、成功するまでtryするものです
func (avatars TryAvatars) GetAvatarURL(u ChatUser) (string, error) {
	for _, avatar := range avatars {
		if url, err := avatar.GetAvatarURL(u); err == nil {
			return url, nil
		}
	}
	return "", ErrNoAvatarURL
}

// UseAuthAvatar は実際に使う AuthAvatarの変数、状態がないので宣言するだけ
var UseAuthAvatar AuthAvatar

// GetAvatarURL はclientからurlを取得するもの // TODO 途中
func (AuthAvatar) GetAvatarURL(u ChatUser) (string, error) {
	url := u.AvatarURL()
	if url != "" {
		return url, nil
	}
	return "", ErrNoAvatarURL
}

// GravatarAvatar とはGravatarというサービスを使用して、サムネを取得するやり方です
type GravatarAvatar struct{}

// UserGravatarAvatar はGravatarAvatarのSingletonです
var UserGravatarAvatar GravatarAvatar

// GetAvatarURL はGravatarからURLを取得するもの
func (GravatarAvatar) GetAvatarURL(u ChatUser) (string, error) {
	return "//www.gravatar.com/avatar/" + u.UniqueID(), nil
}

// FileSystemAvatar は独自のファイルシステムで、URLを取得します
type FileSystemAvatar struct{}

// UserFileSystemAvatar はFileSystemAvatarのSingletonです
var UserFileSystemAvatar FileSystemAvatar

// GetAvatarURL はURLを取得するfunctionです
func (FileSystemAvatar) GetAvatarURL(u ChatUser) (string, error) {
	if files, err := ioutil.ReadDir("avatars"); err == nil {
		for _, file := range files {
			if file.IsDir() {
				continue
			}
			if match, _ := filepath.Match(u.UniqueID()+"*", file.Name()); match {
				return "/avatars/" + file.Name(), nil
			}
		}
	}
	return "", ErrNoAvatarURL
}
