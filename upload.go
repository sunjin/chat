package main

import (
	"io"
	"io/ioutil"
	"net/http"
	"path/filepath"
)

func uploaderHandler(w http.ResponseWriter, req *http.Request) {
	// get id
	userID := req.FormValue("userid")

	// get file
	file, header, err := req.FormFile("avatarFile")

	// check
	if err != nil {
		io.WriteString(w, err.Error())
		return
	}

	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		io.WriteString(w, err.Error())
		return
	}

	// 保存先path生成
	filename := filepath.Join("avatars", userID+filepath.Ext(header.Filename))

	// 書き込み
	err = ioutil.WriteFile(filename, data, 0777)
	if err != nil {
		io.WriteString(w, err.Error())
		return
	}

	io.WriteString(w, "成功")

}
