// 画面ロード時
window.onload = function () {
    this.console.log("Hello Chat World");

    // chat url
    this.setMyChatUi();

}

function setMyChatUi() {
    console.log("=== set my chat url");
    // let img = document.getElementById("headerimg");
    // img.setAttribute('src', this.getMyChatFromCookie());

    let myChatObj = this.getMyChatFromCookie();

    // chat url
    let img = document.getElementById("headerimg");
    img.setAttribute('src', myChatObj["avatar_url"]);

    // chat name
    // let name = document.getElementById("headername");
    // // name.innerHTML = myChatObj["name"];
    // let iosName = myChatObj["name"];
    // let utf8strName = unescape(encodeURIComponent(str));
    // name.innerHTML = utf8strName;
}

// chat 
function getMyChatFromCookie() {
    // get url
    let cookieValue = this.getCookieValie("auth");
    let decodedCookieValue = this.base64Decode(cookieValue);
    let cookieObj = this.jsonParse(decodedCookieValue);
    return cookieObj;
}



// get my chat top from cookie
function getCookieValie(key) {
    console.log("=== get my chat url");
    let r = document.cookie.split(';');
    let result = "";
    r.forEach(function (value) {
        let content = value.split('=');
        if (content[0].trim(" ") === key) {// 空白を取り除く
            result = content[1];
        }
    });
    return result;
}

function base64Decode(encoded) {
    return atob(encoded);
}

// json文字列をobjectでreturnする
function jsonParse(jsonStr) {
    return JSON.parse(jsonStr);
}

function testAlert() {
    alert("TEST OK");
}

// headerのnameを取得する
function getHeaderName() {
    let headerName = document.getElementById('headername').innerHTML;
    return headerName;
}

// headerのurlのsrcを取得する
function getHeaderImgUrl() {
    let headerImgUrl = document.getElementById('headerimg')
    return headerImgUrl.getAttribute('src');
}

// headerのemailを取得する
function getHeaderEmail() {
    return document.getElementById('headeremail').innerHTML;
}

/**
 * date型をフォーマットして、stringで返す
 * @param {String} date 
 */
function dateFormat(beforeDateStr) {

    let date = new Date(beforeDateStr);

    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const hour = date.getHours();
    const minute = date.getMinutes();
    // const second = date.getSeconds();

    const afterDateStr = `${year}年${month}月${day}日 ${hour}:${minute}`;

    return afterDateStr;
}

/**
 * ページを最下部までスクロール
 */
function scrollChatPage() {
    let page = document.getElementById('msgpage');
    page.scrollTop = page.scrollHeight;
}
