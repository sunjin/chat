FROM golang:1.13

LABEL maintaner="Sunjin Yun <sunjin110@gmail.com>"

WORKDIR /gitlab.com/sunjin/chat

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o main .

EXPOSE 8080

CMD ["./main"]