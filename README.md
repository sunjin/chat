# 概要
GolangとDockerを使用して、チャットAPPを作ってみました。  
ログイン認証は、Google OAuth2.0を使用しています。

# 初期配置
1. GoogleのOAuth2.0認証の設定を各自する必要があります。  
認証情報などを登録して、keyなどをmain.goの68行目に入力してください。  


2. https化をする場合は、「createCertificate.sh」のドメインやメールアドレスを変更して、実行してください。

# Deploy
```
$ docker-compose build --no-cache
$ docker-compose up -d
```

# その他
「updateCertificate.sh」は証明書の再発行です、cronなどで実行できるようにしてください。

# 今後やりたいこと
今はチャットの内容は、リロードすると消えてしまうので、  
MongoDBを使用して、チャット内容を非同期で保存できる仕組みを入れたい。

Kubernetesを利用して、オートスケーリングできるようにする  