#!/bin/sh
echo "make start"
docker run --rm \
  -p 443:443 -p 80:80 --name letsencrypt \
  -v "/etc/letsencrypt:/etc/letsencrypt" \
  -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
  certbot/certbot certonly -n \
  -m "sunjin110@gmail.com" \
  -d sunjin.info \
  --standalone --agree-tos