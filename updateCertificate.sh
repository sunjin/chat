#!/bin/sh

docker run --rm --name letsencrypt \
    -v "/etc/letsencrypt:/etc/letsencrypt" \
    -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
    -v "$(cd $(dirname $0) && pwd)/nginx/html:/usr/share/nginx/html" \
    certbot/certbot:latest \
    renew --quiet

# 19 0,12 * * * /root/chat/updateCertificate.sh